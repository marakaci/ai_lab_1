from collections import namedtuple

import numpy
import sys
from PIL import Image, ImageDraw

im = Image.open('img2.jpg')
draw = ImageDraw.Draw(im)

width, height = im.size
pixel_values = list(im.getdata())

BLACK, WHITE = 0, 1
black_pixels = []
visited_pixels = []
figures = []  # array of all black pixels of figures
Point = namedtuple('Point', ['x', 'y'])

for w in range(width):
    black_pixels.append([])
    visited_pixels.append([])
    for h in range(height):
        visited_pixels[w].append(False)
        black_pixels[w].append(sum(pixel_values[w * width + h]) == 0)

# algo iterate through array, track all visited pixels, if pixel is black
# go depth search for surrounding black pixels, write all connected black pixels to array


#
sys.setrecursionlimit(height * width)


# go depth search to spot all
def find_all_connected_pixels(w, h, figure_index):
    if (visited_pixels[w][h]):
        return
    if (black_pixels[w][h]):
        visited_pixels[w][h] = True
        figures[figure_index]['points'].append(Point(x=w, y=h))
        find_all_connected_pixels(w - 1, h + 1, figure_index)
        find_all_connected_pixels(w, h + 1, figure_index)
        find_all_connected_pixels(w + 1, h + 1, figure_index)
        find_all_connected_pixels(w + 1, h, figure_index)
        find_all_connected_pixels(w + 1, h - 1, figure_index)
        find_all_connected_pixels(w, h - 1, figure_index)
        find_all_connected_pixels(w - 1, h - 1, figure_index)
        find_all_connected_pixels(w - 1, h, figure_index)


for w in range(width):
    for h in range(height):
        if (visited_pixels[w][h]):
            continue
        if (black_pixels[w][h]):
            figures.append({'points': []})
            find_all_connected_pixels(w, h, len(figures) - 1)
        visited_pixels[w][h] = True

# 1) определить количество объектов, расположенных на изображении;

print(f'Number of figures on the image is {len(figures)}')


def analyze_figures():
    for figure in figures:
        bottom, left, right, top = height, width, 0, 0
        for point in figure['points']:
            if point.x < left:
                left = point.x
            if point.x > right:
                right = point.x
            if point.y > top:
                top = point.y
            if point.y < bottom:
                bottom = point.y
        draw.line(((bottom - 1, left - 1), (bottom - 1, right + 1)), fill='red', width=2)
        draw.line(((bottom - 1, right + 1), (top + 1, right + 1)), fill='red', width=2)
        draw.line(((top + 1, right + 1), (top + 1, left - 1)), fill='red', width=2)
        draw.line(((top + 1, left - 1), (bottom - 1, left - 1)), fill='red', width=2)
        figure['top'] = top
        figure['left'] = left
        figure['bottom'] = bottom
        figure['right'] = right
        figure['center'] = [(top + bottom) / 2, (left + right) / 2]
        figure['height'] = top - bottom
        figure['width'] = right - left
        figure['square'] = len(figure['points'])


analyze_figures()

# 2) выделить габаритные размеры и определить центры объектов;
# 3) для каждого объекта отобразить т.н. окно "захвата" объекта и центр объекта;
# 4) в таблицу вывести значения площадей S и центров (x, y) объектов;
# 5) определить следующие параметры:
# − минимальное значение площади S объекта O(min) и его центр,
# − максимальное значение площади S объекта O(max) и его центр.
print()
for f in figures:
    print(f"top {f['top']}, bottom {f['bottom']}, left {f['left']}, right {f['right']}")
    print(f"height: {f['height']}, width: {f['width']}, center: {f['center']}, square: {f['square']}")
    print('------------')
im.save('out2.jpg', 'JPEG')
